// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {describe, it} from "mocha";
import assert from "assert";
import jsc from "jsverify";

import {Identity, identity, sum} from "../test-utils/arbitrary.js";

import {id} from "@fpjs/overture/base";
import {runIdentity} from "@fpjs/overture/data/identity";

import {map} from "@fpjs/overture/algebras/functor";
import {chain, isChain, join} from "@fpjs/overture/algebras/chain";

describe ("Chain", () => {
    describe("chain", () => {
        jsc.property("sequentially compose", identity(jsc.string), jsc.fn(identity(jsc.nat)),
                     (x, y) => runIdentity (chain (y) (x)) === runIdentity (y (runIdentity (x))));

        jsc.property("throws on non-chain", jsc.oneof(identity(jsc.nat), jsc.record({x: jsc.nat})),
                     (x) => jsc.throws(() => chain (Identity) (x), TypeError, "'Object' is not an Chain.") !== isChain (x));
    });

    describe("join", () => {
        jsc.property("remove one level of monadic structure", identity(identity(jsc.string)),
                     (x) => runIdentity (join (x)) === runIdentity (map (runIdentity) (x)));
    });

    describe("isChain", () => {
        jsc.property("identity monad", identity(jsc.nat),
                     (x) => isChain (x));

        jsc.property("non-chain", sum(jsc.nat),
           (x) => ! isChain (x));

        it("requires an apply", () => {
            const Absurd = (x) => ({
                "fantasy-land/chain": (f) => f (x),
            });
            assert (! isChain (Absurd (id)));
        });
    });
});
