// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {describe} from "mocha";
import jsc from "jsverify";

import {identity, sum} from "../test-utils/arbitrary.js";

import {constant, flip} from "@fpjs/overture/base";
import {lte} from "@fpjs/overture/algebras/ord";
import {equals} from "@fpjs/overture/algebras/setoid";
import {runIdentity} from "@fpjs/overture/data/identity";

import {isFoldable, foldl, foldMap, filter, filterOut} from "@fpjs/overture/algebras/foldable";

describe ("Foldable", () => {
    const snd = flip (constant);

    describe("foldl", () => {
        jsc.property("combine the elements of a structure", identity(jsc.string),
                     (x) => foldl (snd) ("") (x) === runIdentity (x));

        jsc.property("throws on non-foldable", jsc.oneof(identity(jsc.string), jsc.record({x: jsc.string})),
                     (x) => jsc.throws(
                         () => foldl (snd) ("") (x), TypeError, "'Object' is not Foldable.") !== isFoldable (x)
                    );
    });

    describe("isFoldable", () => {
        jsc.property("identity functor", identity(jsc.nat),
                     (x) => isFoldable (x));

        jsc.property("non-foldable", sum(jsc.nat),
           (x) => ! isFoldable (x));
    });

    describe("foldMap", () => {
        jsc.property("identity", jsc.array(jsc.nat),
                     (xs) => equals (foldMap (Array) (x => [x]) (xs)) (xs));
    });

    describe("filter", () => {
        jsc.property("zero", jsc.array(jsc.nat),
                     (xs) => equals (filter (Array) (constant (false)) (xs)) ([]));

        jsc.property("identity", jsc.array(jsc.nat),
                     (xs) => equals (filter (Array) (constant (true)) (xs)) (xs));

        jsc.property("size", jsc.array(jsc.nat), jsc.nat,
                     (xs, y) => (filter (Array) (lte (y)) (xs)).length <= xs.length);
    });

    describe("filterOut", () => {
        jsc.property("zero", jsc.array(jsc.nat),
                     (xs) => equals (filterOut (Array) (constant (true)) (xs)) ([]));

        jsc.property("identity", jsc.array(jsc.nat),
                     (xs) => equals (filterOut (Array) (constant (false)) (xs)) (xs));

        jsc.property("size", jsc.array(jsc.nat), jsc.nat,
                     (xs, y) => (filterOut (Array) (lte (y)) (xs)).length <= xs.length);
    });
});
