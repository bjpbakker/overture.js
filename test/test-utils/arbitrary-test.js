// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {describe} from "mocha";
import jsc from "jsverify";

import {type} from "@fpjs/overture/base";
import {runIdentity} from "@fpjs/overture/data/identity";

import {identity, maybe, sum} from "./arbitrary.js";

describe("arbitrary identity", () => {
    jsc.property("generates identity", identity(jsc.nat),
                 (x) => typeof (runIdentity (x)) === "number");
});


describe("arbitrary maybe", () => {
    jsc.property("generates maybe", maybe(jsc.nat),
                 (x) => type (x) === "Maybe");
});

describe("arbitrary sum", () => {
    jsc.property("generates sum", sum(jsc.nat),
                 (x) => typeof (x.getSum) === "number");
});
