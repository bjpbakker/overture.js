// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

/**
 * Monoid is a semigroup with an identity element.
 */

import {typerep} from "@fpjs/overture/base";

///:: a -> boolean
export const isMonoid = (x) => "fantasy-land/empty" in typerep (x);

///:: Monoid a => TypeRep a -> a
export const empty = (A) => {
    const op = A["fantasy-land/empty"];
    if (op === undefined) {
        throw TypeError(`'${A.name}' is not a Monoid.`);
    }
    return op();
};

export {concat} from "@fpjs/overture/algebras/semigroup";
