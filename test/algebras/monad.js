// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {describe, it} from "mocha";
import assert from "assert";
import jsc from "jsverify";

import {identity} from "../test-utils/arbitrary.js";

import {id, typerep} from "@fpjs/overture/base";
import {runIdentity} from "@fpjs/overture/data/identity";

import {chain, isMonad, pure} from "@fpjs/overture/algebras/monad";

describe ("Monad", () => {
    describe("chain", () => {
        jsc.property("compose sequential operation", identity(jsc.string),
                     (x) => runIdentity (chain (pure (typerep (x))) (x)) === runIdentity (x));
    });

    describe("isMonad", () => {
        jsc.property("identity monad", identity(jsc.string),
                     (x) => isMonad (x));

        it ("requires an applicative functor", () => {
            const Absurd = (x) => ({
                "fantasy-land/chain": (f) => f (x),
            });
            assert (! isMonad (Absurd (id)));
        });

    });
});
