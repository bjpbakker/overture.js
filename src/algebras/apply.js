// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

/**
 * Apply sequences functorial computations and combines their result. An
 * instance of Apply must also implement the Functor specification.
 */

import {type} from "@fpjs/overture/base";
import {isFunctor} from "@fpjs/overture/algebras/functor";

///:: a -> boolean
export const isApply = (x) => isFunctor(x) && "fantasy-land/ap" in x;

///:: Apply f => f (a -> b) -> f a -> f b
export const ap = (ab) => (a) => {
    const op = a["fantasy-land/ap"];
    if (op === undefined) {
        throw TypeError(`'${type(a)}' is not an Apply.`);
    }
    return op.call(a, ab);
};
