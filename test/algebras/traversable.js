// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {describe} from "mocha";
import jsc from "jsverify";

import {Identity, identity, maybe, sum} from "../test-utils/arbitrary.js";

import {map} from "@fpjs/overture/algebras/functor";
import {equals} from "@fpjs/overture/algebras/setoid";
import {runIdentity} from "@fpjs/overture/data/identity";
import {Maybe, Just} from "@fpjs/overture/data/maybe";

import {isTraversable, sequence, traverse} from "@fpjs/overture/algebras/traversable";

describe ("Traversable", () => {
    describe("traverse", () => {
        jsc.property("map element to action", identity(jsc.string),
                     (x) => equals (traverse (Maybe) (Just) (x)) (Just (x)));

        jsc.property("throws on non-traversable", jsc.oneof(identity(jsc.string), jsc.record({x: jsc.string})),
                     (x) => jsc.throws(
                         () => traverse (Maybe) (Just) (x), TypeError, "'Object' is not Traversable.") !== isTraversable (x)
                    );
    });

    describe("sequence", () => {
        jsc.property("collect results", identity(maybe(jsc.string)),
                     (x) => equals (sequence (Maybe) (x)) (map (Identity) (runIdentity (x))));
    });

    describe("isTraversable", () => {
        jsc.property("identity functor", identity(jsc.nat),
                     (x) => isTraversable (x));

        jsc.property("non-traversable", sum(jsc.nat),
           (x) => ! isTraversable (x));
    });
});
