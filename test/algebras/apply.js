// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {describe, it} from "mocha";
import assert from "assert";
import jsc from "jsverify";

import * as A from "../test-utils/arbitrary.js";

import {id} from "@fpjs/overture/base";
import {Identity, runIdentity} from "@fpjs/overture/data/identity";

import {ap, isApply} from "@fpjs/overture/algebras/apply";

describe ("Apply", () => {
    describe("ap", () => {
        jsc.property("combine computation sequentially", A.identity(jsc.string),
                     (x) => runIdentity (ap (Identity(id)) (x)) === runIdentity (x));

        jsc.property("throws on non-apply", jsc.oneof(A.identity(jsc.nat), jsc.record({x: jsc.nat})),
                     (x) => jsc.throws(() => ap (Identity (id)) (x), TypeError, "'Object' is not an Apply.") !== isApply (x));
    });

    describe("isApply", () => {
        jsc.property("identity functor", A.identity(jsc.nat),
                     (x) => isApply (x));

        jsc.property("non-apply", A.sum(jsc.nat),
           (x) => ! isApply (x));

        it("requires a functor", () => {
            const Absurd = (x) => ({
                "fantasy-land/ap": (f) => Absurd (f (x)),
            });
            assert (! isApply (Absurd (id)));
        });
    });
});
