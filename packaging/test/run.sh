#!/bin/sh

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -euf

cd "$(dirname $0)"

node --input-type=module <<EOF
import assert from "assert";
import {id} from "@fpjs/overture/base";

const x = "X";
assert (id (x) === x);
EOF

node --input-type=commonjs <<EOF
const assert = require("assert");
const {id} = require("@fpjs/overture/base");

const x = "X";
assert (id (x) === x);
EOF
