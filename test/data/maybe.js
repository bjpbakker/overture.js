// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {describe, it} from "mocha";
import {assert} from "chai";
import jsc from "jsverify";

import {maybe, nullish} from "../test-utils/arbitrary.js";
import {inc} from "../test-utils/functions.js";

import {compose, type} from "@fpjs/overture/base";
import {map} from "@fpjs/overture/algebras/functor";
import {ap, pure} from "@fpjs/overture/algebras/applicative";
import {chain} from "@fpjs/overture/algebras/monad";
import {eq, ineq, isSetoid} from "@fpjs/overture/algebras/setoid";
import {gt, isOrd, lt} from "@fpjs/overture/algebras/ord";

import {
    Maybe, Just, Nothing,
    fromMaybe, toMaybe, maybeToArray,
} from "@fpjs/overture/data/maybe";

describe ("Maybe", () => {
    jsc.property("type", maybe(jsc.string),
                 (x) => type (x) === "Maybe");

    jsc.property("Functor", maybe(jsc.nat),
                 (x) => fromMaybe (0) (map (inc) (x)) === inc (fromMaybe (-1) (x)));

    jsc.property("Apply", maybe(jsc.nat),
                 (x) => fromMaybe (0) (ap (Just (inc)) (x)) === inc (fromMaybe (-1) (x)));

    jsc.property("Applicative", "nat",
                 (n) => fromMaybe (0) (pure (Maybe) (n)) === n);

    jsc.property("Monad", maybe(jsc.nat),
                 (x) => fromMaybe (0) (chain (compose (Just) (inc)) (x)) === inc (fromMaybe (-1) (x)));

    describe ("Setoid", () => {
        jsc.property("isSetoid", maybe(jsc.nat), isSetoid);

        jsc.property("associativity", maybe(jsc.nat), maybe(jsc.nat),
                     (x, y) => eq (x) (y) === eq (y) (x));

        jsc.property("Nothing ≢ Just", jsc.nat,
                     (x) => ineq (Nothing) (Just (x)) && ineq (Just (x)) (Nothing));
    });

    describe ("Ord", () => {
        jsc.property("isOrd", maybe(jsc.nat), isOrd);

        jsc.property("associativity", maybe(jsc.nat), maybe(jsc.nat),
                     (x, y) => lt (x) (y) !== gt (x) (y) || eq (x) (y));

        jsc.property("Nothing < Just x", jsc.nat,
                     (x) => lt (Nothing) (Just (x)));
    });

    describe ("fromMaybe", () => {
        jsc.property("extracts value from Just", jsc.nat,
                     (x) => fromMaybe (null) (Just (x)) === x);

        jsc.property("returns default when given Nothing", jsc.nat,
                     (x) => fromMaybe (x) (Nothing) === x);

        it ("throws TypeError for non-Maybe",
            () => assert.throws(() => fromMaybe (0) ({}), TypeError));
    });

    describe ("toMaybe", () => {
        jsc.property("wraps value in Just", jsc.nat,
                     (x) => eq (Just (x)) (toMaybe (x)));

        jsc.property("turns nullish into Nothing", nullish,
                     (none) => toMaybe (none) === Nothing);
    });

    jsc.property("fromMaybe ∘ toMaybe ≡ id", jsc.nat,
                 (x) => fromMaybe (null) (toMaybe (x)) === x);


    describe ("maybeToArray", () => {
        it("returns empty when given Nothing",
           () => assert.deepEqual(maybeToArray (Nothing), []));

        jsc.property("returns singleton array when given Just", jsc.nat,
                     (x) => eq (maybeToArray (Just (x))) ([x]));

        it ("throws TypeError for non-Maybe",
            () => assert.throws(() => maybeToArray ({}), TypeError));
    });
});
