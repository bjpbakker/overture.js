// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import jsc from "jsverify";

import {Identity, runIdentity} from "@fpjs/overture/data/identity";
import {Just, Nothing, fromMaybe, toMaybe} from "@fpjs/overture/data/maybe";
import {map} from "@fpjs/overture/algebras/functor";

export {Identity} from "@fpjs/overture/data/identity";

export const identity = (a) => ({
    generator: jsc.generator.bless((size) => Identity (a.generator (size))),
    shrink: jsc.shrink.bless((x) => a.shrink(runIdentity (x)).map(Identity)),
    show: (x) => `Identity ${runIdentity (x)}`,
});

export const maybe = (a) => ({
    generator: jsc.generator.bless((size) => size % 2 === 0 ? Just (a.generator (size)) : Nothing),
    shrink: jsc.shrink.bless((x) => a.shrink (fromMaybe (a.generator(0)) (x)).map(toMaybe)),
    show: (x) => fromMaybe ("Nothing") (map ((v) => `Just ${v}`) (x)),
});

export const nullish = jsc.oneof(jsc.constant(null), jsc.constant(undefined));

export const Sum = (x) => ({
    "@@type": "Sum",
    "constructor": Sum,
    "getSum": x,
    "fantasy-land/concat": (y) => Sum (x + y.getSum),
    "fantasy-land/equals": (y) => x === y.getSum,
    "fantasy-land/lte": (y) => x <= y.getSum,
});
Sum["fantasy-land/empty"] = () => Sum (0);

export const sum = (a) => {
    return jsc.bless({
        generator: jsc.generator.bless((size) => Sum (a.generator (size))),
        shrink: jsc.shrink.bless((x) => a.shrink(x.getSum).map(Sum)),
        show: (x) => `Sum ${x.getSum}`,
    });
};
