// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {describe} from "mocha";
import jsc from "jsverify";

import {sum} from "../test-utils/arbitrary.js";

import {eq, ineq} from "@fpjs/overture/algebras/setoid";
import {isOrd, lt, lte, gte, gt, min, max, clamp} from "@fpjs/overture/algebras/ord";

describe ("Ord", () => {
    describe("lte", () => {
        jsc.property("ordering", sum(jsc.nat), sum(jsc.nat),
                     (x, y) => lte (x) (y) === (x.getSum <= y.getSum));

        jsc.property("throws on non-ord", jsc.oneof(sum(jsc.nat), jsc.record({x: "nat"})),
                     (x) => jsc.throws(() => lte (x) (x), TypeError, "'Object' is not an Ord.") !== isOrd(x));

    });

    describe("lt", () => {
        jsc.property("ordering", sum(jsc.nat), sum(jsc.nat),
                     (x, y) => lt (x) (y) === (x.getSum < y.getSum));
    });

    describe("gte", () => {
        jsc.property("ordering", sum(jsc.nat), sum(jsc.nat),
                     (x, y) => gte (x) (y) === (x.getSum >= y.getSum));
    });

    describe("gt", () => {
        jsc.property("ordering", sum(jsc.nat), sum(jsc.nat),
                     (x, y) => gt (x) (y) === (x.getSum > y.getSum));
    });

    describe("isOrd", () => {
        jsc.property("value w/ lte", sum(jsc.nat),
                     (x) => isOrd (x));

        jsc.property("value w/o lte", jsc.record({x: "json"}),
                     (x) => ! isOrd (x));
    });

    describe("primitive derivation", () => {
        jsc.property("derive primitive ordering", "nat", "nat",
                     (x, y) => lte (x) (y) === (x <= y));

        jsc.property("isOrd", jsc.oneof(jsc.nat, jsc.string, jsc.bool),
                     (x) => isOrd (x));
    });

    describe("min", () => {
        jsc.property("lesser value", jsc.nat, jsc.nat,
                     (x, y) => ineq (min (x) (y)) (max (x) (y)) || eq (x) (y));
    });

    describe("max", () => {
        jsc.property("greater value", jsc.nat, jsc.nat,
                     (x, y) => ineq (max (x) (y)) (min (x) (y)) || eq (x) (y));
    });

    describe("clamp", () => {
        jsc.property("boundaries", jsc.nat, jsc.nat,
                     (x, y) => clamp (x) (x) (y) === x);

        jsc.property("in range", jsc.nat, jsc.nat,
                     (x, y) => clamp (x) (x + y) (x + y / 2) === x + y / 2);
    });
});
