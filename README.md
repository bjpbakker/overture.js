# Overture

A Javascript library that provides the very basics for functional programming.

Overture aims to be fully compatible with the [Fantasy Land
Specification](https://github.com/fantasyland/fantasy-land), but does not
yet implement the complete specification.

## Getting Started

The simplest way to explore is by using the `repl` that comes with
`@fpjs/overture`.

```
> npx --package='@fpjs/overture' -c repl
```

This puts you in a repl with all overture modules globally available. Use
[dynamic import](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/import)
to import other modules.

``` js
λ const { myExport } = await import("./path/to/file.js")
```

## Upgrading

A changelog is kept in `RELEASES.md`, which lists the major changes.

## Usage

Overture provides multiple modules to allow you to import just the ones you
actually use. All imports are relative to the package root `@fpjs/overture`.

```js
import {id} from "@fpjs/overture/base";
```

### Algebras

Algebras are implemented in `@fpjs/overture/algebras`. Each algebra exports its
related functions, causing some functions being exported by multiple algebras
(e.g. `ap`, which is exported by `Apply` and `Applicative`).

### Data structures

Some common data structures such as `Identity` and `Maybe` are implemented in
`@fpjs/overture/data`.

### Callback functor

Node.js-style callback functions are notoriously cumbersome to deal with. The
`Callback` functor (`@fpjs/overture/control/callback`) wraps callback-style
functions and provides `Functor`, `Applicative` and `Monad` instances. This
allows to simply deal with the data in a callback.

### Lens

Lenses ease (nested) record attribute access. An implemention is provides in `@fpjs/overture/control/lens`.

### Built-ins

To use the implementations of various algebras for built-ins (like `Array` and
`Promise`), the built-in classes can be extended (or: [monkey
patched](https://en.wikipedia.org/wiki/Monkey_patch)). This is always opt-in; no
modules have side effects just on importing them.

```js
import {patchBuiltins} from "@fpjs/overture/patches";
...
patchBuiltins();
```

## Fantasy Land compatibility

### Algebras

- [X] Setoid
- [X] Ord
- [ ] Semigroupoid
- [ ] Category
- [X] Semigroup
- [X] Monoid
- [ ] Group
- [ ] Filterable
- [X] Functor
- [ ] Contravariant
- [X] Apply
- [X] Applicative
- [ ] Alt
- [ ] Plus
- [ ] Alternative
- [X] Foldable
- [X] Traversable
- [X] Chain
- [ ] ChainRec
- [X] Monad
- [ ] Extend
- [ ] Comonad
- [ ] Bifunctor
- [ ] Profunctor

### Derivations

- [X] derive `fantasy-land/equals` for Ord
- [ ] derive `fantasy-land/map` for Applicative
- [ ] derive `fantasy-land/map` for Monad
- [ ] derive `fantasy-land/map` for Bifunctor
- [ ] derive `fantasy-land/ap` for Chain

## License

Copyright 2019-2023, Bart Bakker.

This software is subject to the terms of the Mozilla Public License, v. 2.0. If
a copy of the MPL was not distributed with this file, You can obtain one at
http://mozilla.org/MPL/2.0/.
