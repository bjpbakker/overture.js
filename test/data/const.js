// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {describe, it} from "mocha";
import {assert} from "chai";
import jsc from "jsverify";

import {inc} from "../test-utils/functions.js";

import {type} from "@fpjs/overture/base";
import {map} from "@fpjs/overture/algebras/functor";
import {ap, pure} from "@fpjs/overture/algebras/applicative";
import {equals} from "@fpjs/overture/algebras/setoid";
import {lte} from "@fpjs/overture/algebras/ord";

import {Const, getConst} from "@fpjs/overture/data/const";

describe ("Const", () => {
    jsc.property("type", jsc.oneof(jsc.nat, jsc.string),
                 (x) => type (Const (x)) === "Const");

    jsc.property("getConst", "json",
                 (x) => getConst (Const (x)) === x);

    jsc.property("Functor", "nat",
                 (n) => getConst (map (inc) (Const (n))) === n);

    jsc.property("Apply", "nat",
                 (n) => getConst (ap (Const (inc)) (Const (n))) === n);

    jsc.property("Applicative", "nat",
                 (n) => getConst (pure (Const) (n)) === n);

    jsc.property("Setoid", "nat",
                 (n) => equals (Const (n)) (Const (n)));

    jsc.property("Ord", "nat",
                 (n) => lte (Const (n)) (Const (n)));

    it ("getConst throws TypeError for non-Const",
        () => assert.throws(() => getConst ({}), TypeError));
});
