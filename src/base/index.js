// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

///:: a -> a
export const id = (x) => x;

///:: (b -> c) -> (a -> b) -> a -> c
export const compose = (f) => (g) => (x) => f (g (x));

/** `show` for types of values.  */
///:: a -> string
export const type = (x) => {
    if (typeof (x) === "object" && "@@type" in x) {
        return x["@@type"];
    }
    return Object.prototype.toString.call(x).slice("[object ".length, -"]".length);
};

///:: TypeRep t => a -> t a
export const typerep = (x) => x.constructor;

///:: a -> boolean
export const isPrimitive = (x) =>
    x == null || (typeof(x) !== "object" && typeof(x) !== "function");

///:: ((a, b) -> c) -> a -> b -> c
export const curry = (f) => (x) => (y) => f(x, y);

///:: (a -> b -> c) -> (a, b) -> c
export const uncurry = (f) => (x, y) => f (x) (y);

///:: (a -> b -> c) -> b -> a -> c
export const flip = (f) => (y) => (x) => f (x) (y);

/**
 * Application operation. Mostly this is redundant since
 * `apply (f) (x) ≡ f (x)`, but it can be useful in higher-order situations.
 */
///:: (a -> b) -> a -> b
export const apply = (f) => f;

/** Reverse application operation. */
///:: a -> (a -> b) -> b
export const rapply = flip (apply);

///:: a -> b -> a;
export const constant = (x) => (_) => x;
export const always = constant;

/** Boolean not */
///:: bool -> bool
export const not = (x) => !x;

///:: (a -> b) -> (a -> c) -> a -> (b, c)
export const fanout = (f) => (g) => (x) => [f (x), g (x)];
