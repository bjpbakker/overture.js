// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

/**
 * Applicative functors embed (lift) pure expressions, and sequence functorial
 * computations. Hence an instance of Applicative must also implement the Apply
 * specification.
 */

import {typerep} from "@fpjs/overture/base";
import {ap, isApply} from "@fpjs/overture/algebras/apply";
import {map} from "@fpjs/overture/algebras/functor";

export {ap} from "@fpjs/overture/algebras/apply";

///:: a -> boolean
export const isApplicative = (x) => isApply (x) && "fantasy-land/of" in typerep (x);

///:: Applicative f => TypeRep f -> a -> f a
export const pure = (F) => (x) => {
    const op = F["fantasy-land/of"];
    if (op === undefined) {
        throw TypeError(`'${F.name}' is not an Applicative.`);
    }
    return op(x);
};

export const of = pure;

///:: Apply f => (a -> b) -> f a -> f b
export const lift = (ab) => (a) => ap (pure (typerep (a)) (ab)) (a);

///:: Apply f => (a -> b -> c) -> f a -> f b -> f c
export const lift2 = (abc) => (a) => (b) => ap (map (abc) (a)) (b);
