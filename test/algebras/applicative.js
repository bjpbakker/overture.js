// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {describe, it} from "mocha";
import jsc from "jsverify";

import * as A from "../test-utils/arbitrary.js";
import {inc, sum} from "../test-utils/functions.js";

import {id, typerep} from "@fpjs/overture/base";
import {Identity, runIdentity} from "@fpjs/overture/data/identity";

import {ap, of, pure, lift, lift2, isApplicative} from "@fpjs/overture/algebras/applicative";

describe ("Applicative", () => {
    describe("pure", () => {
        jsc.property("embed pure expression", A.identity(jsc.string),
                     (x) => runIdentity (ap (pure (Identity) (id)) (x)) === runIdentity (x));

        jsc.property("throws on non-applicative", jsc.oneof(A.identity(jsc.nat), jsc.record({x: jsc.nat})),
                     (x) => jsc.throws(
                         () => pure (typerep (x)) (id), TypeError, "'Object' is not an Applicative.") !== isApplicative (x));
    });

    describe("of", () => {
        it("is alias of pure", () => of === pure);
    });

    describe("isApplicative", () => {
        jsc.property("typerep w/ lift", jsc.oneof(A.identity(jsc.nat), jsc.record({x: jsc.nat})),
                     (x) => isApplicative (x) === (typerep (x) === Identity));
    });

    describe("lift", () => {
        jsc.property("lift a function", A.identity(jsc.nat),
                     (x) => runIdentity (lift (inc) (x)) === inc (runIdentity (x)));
    });

    describe("lift2", () => {
        jsc.property("lift a binary function", A.identity(jsc.nat), A.identity(jsc.nat),
                     (x, y) => runIdentity (lift2 (sum) (x) (y)) === runIdentity (x) + runIdentity (y));
    });
});
