// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

/**
 * The Const functor.
 */

import {type} from "@fpjs/overture/base";
import {equals} from "@fpjs/overture/algebras/setoid";
import {lte} from "@fpjs/overture/algebras/ord";

const self = function() { return this; };

/** Lift a value in the Const functor. */
///:: a -> Const a k
export const Const = (x) => ({
    "@@type": "Const",
    "constructor": Const,
    "getConst": x,
    "fantasy-land/map": self,
    "fantasy-land/ap": self,
    "fantasy-land/equals": (y) => equals (x) (y.getConst),
    "fantasy-land/lte": (y) => lte (x) (y.getConst),
});
Const["fantasy-land/of"] = Const;

/** Extract the value from a Const functor. */
///:: Const a k -> k
export const getConst = (o) => {
    if (! ("getConst" in o)) {
        throw TypeError(`${type(o)} is not Const`);
    }
    return o["getConst"];
};
