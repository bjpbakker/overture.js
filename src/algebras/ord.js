// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

/**
 * Ord is a set equipped with a total order relation. An instance must provide a
 * `fantasy-land/lte` method. Any `Ord` is automatically also a `Setoid`.
 */

import {isPrimitive, type} from "@fpjs/overture/base";

///:: a -> boolean
export const isOrd = (x) => isPrimitive (x) || "fantasy-land/lte" in x;

///:: Ord a => a -> a -> boolean
export const lte = (x) => (y) => {
    if (isPrimitive (x)) {
        return x <= y;
    }

    const op = x["fantasy-land/lte"];
    if (op === undefined) {
        throw TypeError(`'${type(x)}' is not an Ord.`);
    }
    return x === y || op.call(x, y);
};

///:: Ord a => a -> a -> boolean
export const lt = (x) => (y) => {
    return ! lte (y) (x);
};

///:: Ord a => a -> a -> boolean
export const gte = (x) => (y) => {
    return lte (y) (x);
};

///:: Ord a => a -> a -> boolean
export const gt = (x) => (y) => {
    return ! lte (x) (y);
};

///:: Ord a => a -> a -> a
export const min = (x) => (y) =>
    gt (x) (y) ? y : x;

///:: Ord a => a -> a -> a
export const max = (x) => (y) =>
    gt (x) (y) ? x : y;

/**
 * Truncate value so that it stays in range.
 *
 * >>> clamp (5) (10) (1)
 * 5
 * >>> clamp (5) (10) (7)
 * 7
 * >>> clamp (5) (10) (11)
 * 10
 */
///:: Ord a => a -> a -> a -> a
export const clamp = (mn) => (mx) => (val) =>
    max (mn) (min (mx) (val));
