import js from "@eslint/js";
import stylisticJs from "@stylistic/eslint-plugin-js";

export default [
    js.configs.recommended,
    {
        plugins: {
            "@stylistic/js": stylisticJs,
        },
        rules: {
            "@stylistic/js/indent": ["error", 4],
            "@stylistic/js/semi": ["error", "always"],
            "comma-dangle": ["error", {
                "arrays": "always-multiline",
                "objects": "always-multiline",
                "imports": "always-multiline",

                "exports": "always-multiline",
                "functions": "never",
            }],
            "no-empty-functions": "off",
            "no-unexpected-multiline": "off",
            "no-unused-vars": ["error", {"argsIgnorePattern": "^_"}],
            "quotes": ["error", "double", {
                avoidEscape: true,
            }],
        },
    },
    {
        files: ["test/**/*.js"],
        rules: {
            "@stylistic/js/indent": "off",
        },
    },
];
