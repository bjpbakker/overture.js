// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {describe} from "mocha";
import jsc from "jsverify";

import {Sum, sum} from "../test-utils/arbitrary.js";

import {typerep} from "@fpjs/overture/base";
import {concat, empty, isMonoid} from "@fpjs/overture/algebras/monoid";

describe ("Monoid", () => {
    describe("empty", () => {
        jsc.property("identity element", sum(jsc.nat),
                     (x) => (concat (empty (Sum)) (x)).getSum === x.getSum);

        jsc.property("throws on non-monoid", jsc.oneof(sum(jsc.nat), jsc.record({x: jsc.nat})),
                     (x) => jsc.throws(() => empty (typerep (x)), TypeError, "'Object' is not a Monoid.") !== isMonoid (x));
    });

    describe("isMonoid", () => {
        jsc.property("typerep w/ identity element", jsc.oneof(sum(jsc.nat), jsc.record({x: jsc.nat})),
                     (x) => isMonoid (x) === (typerep (x) === Sum));
    });
});
