// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {describe, it} from "mocha";
import {assert} from "chai";
import jsc from "jsverify";

import {identity} from "../test-utils/arbitrary.js";
import {inc} from "../test-utils/functions.js";

import {type} from "@fpjs/overture/base";
import {map} from "@fpjs/overture/algebras/functor";
import {ap, pure} from "@fpjs/overture/algebras/applicative";
import {chain} from "@fpjs/overture/algebras/monad";
import {equals} from "@fpjs/overture/algebras/setoid";
import {lte} from "@fpjs/overture/algebras/ord";

import {Identity, runIdentity} from "@fpjs/overture/data/identity";

describe ("Identity", () => {
    jsc.property("type", jsc.oneof(jsc.nat, jsc.string),
                 (x) => type (Identity (x)) === "Identity");

    jsc.property("runIdentity", "json",
                 (x) => runIdentity (Identity (x)) === x);

    jsc.property("Functor", "nat",
                 (n) => runIdentity (map (inc) (Identity (n))) === n + 1);

    jsc.property("Apply", "nat",
                 (n) => runIdentity (ap (Identity (inc)) (Identity (n))) === inc (n));

    jsc.property("Applicative", "nat",
                 (n) => runIdentity (pure (Identity) (n)) === n);

    jsc.property("Monad", "nat",
                 (n) => runIdentity (chain (Identity) (Identity (n))) === n);

    jsc.property("Setoid", identity(jsc.nat),
                 (x) => equals (x) (chain (Identity) (x)));

    jsc.property("Ord", identity(jsc.nat),
                 (x) => lte (x) (chain (Identity) (x)));

    it ("runIdentity throws TypeError for non-Identity",
        () => assert.throws(() => runIdentity ({}), TypeError));
});
