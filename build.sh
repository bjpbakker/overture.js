#!/bin/sh

die() {
    echo "$@"
    exit 1
}

run() {
    if [ -z "$npm_package_name" ]; then
        npx --no-install "$@"
    else
        "$@"
    fi
}

pristine=0
clean=0

for arg in "$@"; do
    case $arg in
        --clean )
            clean=1 ;;
        --pristine )
            pristine=1 ;;
        * )
            die "Unknown argument: $arg"
    esac
done

if [ $pristine -ne 0 ]; then
    test -z "$(git status --porcelain --untracked-files=no)" || die "Cannot proceed with dirty working dir"
fi

if [ $clean -ne 0 ]; then
    rm -rf dist/
fi

mkdir -p dist/cjs
echo '{ "type": "commonjs" }' > dist/cjs/package.json

run babel src --extensions '.js' --out-dir dist/cjs
