// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

/**
 * Setoid is a set equipped with an equivalence relation. An instance must
 * either provide a `fantasy-land/equals` method or implement the `Ord`
 * specification.
 *
 * Instances of `Ord` may choose to also provide a `fantasy-land/equals` method
 * to provide a more efficient implementation.
 */

import {isPrimitive, type} from "@fpjs/overture/base";
import {isOrd, lte} from "@fpjs/overture/algebras/ord";

///:: a -> boolean
export const isSetoid = (x) => isOrd (x) || "fantasy-land/equals" in x;

/** Test for equality. */
///:: Setoid a => a -> a -> boolean
export const equals = (x) => (y) => {
    if (isPrimitive (x)) {
        return x === y;
    }

    const op = x["fantasy-land/equals"];
    if (op === undefined) {
        if (isOrd (x)) {
            return lte (x) (y) && lte (y) (x);
        } else {
            throw TypeError(`'${type(x)}' is not a Setoid.`);
        }
    }
    return x === y || op.call(x, y);
};

export const eq = equals;

/** Test for inequality. */
///:: Setoid a => a -> a -> boolean
export const ineq = (x) => (y) => ! eq (x) (y);
