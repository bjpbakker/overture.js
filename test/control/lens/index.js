// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {describe} from "mocha";
import jsc from "jsverify";

import {compose} from "@fpjs/overture/base";
import {equals} from "@fpjs/overture/algebras/setoid";
import {patchBuiltins} from "@fpjs/overture/patches";

import * as L from "@fpjs/overture/control/lens";

patchBuiltins();

describe ("Lens", () => {
    const l = L.identity;

    jsc.property("view", "json",
                 (x) => L.view (l) (x) === x);

    jsc.property("set", "json", "json",
                 (s, v) => L.set (l) (v) (s) === v);

    jsc.property("over", "fun nat", "nat",
                 (f, n) => L.over (l) (f) (n) === f (n));

    jsc.property("view l (set l v s) ≡ v", "json", "json",
                 (s, v) => L.view (l) (L.set (l) (v) (s)) === v);

    jsc.property("composable", "json",
                 (x) => L.view (compose (l) (l)) (x) === x);
});

describe ("Combinators", () => {
    const l = L.identity;

    jsc.property("setAll", "nat", "nat", "nat",
                 (x, y, z) => L.setAll ([l, x], [l, y]) (z) === y);
});

describe ("Tuple accessors", () => {
    const {_1, _2} = L.Tuple;

    jsc.property("view", "pair nat nat",
                 ([x, y]) => L.view (_1) ([x, y]) === x);

    jsc.property("set", "pair nat nat", "nat",
                 ([x, y], z) => equals ([x, z]) (L.set (_2) (z) ([x, y])));

    jsc.property("composable", "pair nat nat", "nat",
                 ([x, y], z) => L.view (compose (_1) (_2)) ([[x, y], z]) === y);
});

describe ("Record", () => {
    const _ = L.Record;

    jsc.property("view", jsc.record({a: "nat", b: "nat"}),
                 (rec) => L.view (_.a) (rec) ===  rec.a);

    jsc.property("set", jsc.record({a: "nat", b: "nat"}), "nat",
                 (rec, x) => L.view (_.a) (L.set (_.a) (x) (rec)) ===  x);

    jsc.property("composable", jsc.record({a: jsc.record({b: "nat"})}),
                 (rec) => L.view (compose (_.a) (_.b)) (rec) === rec.a.b);


    jsc.property("nested accessors", jsc.record({a: jsc.record({b: "nat"})}),
                 (rec) => L.view (_.a.b) (rec) === rec.a.b);
});

describe ("Indexed", () => {
    const ix = L.Indexed.ix;

    jsc.property("view", "nearray nat",
                 (xs) => L.view (ix (3)) (xs) === xs[3]);

    jsc.property("set", "nearray nat", "nat",
                 (xs, y) => L.view (ix (0)) (L.set (ix (0)) (y) (xs)) === y);

    jsc.property("composable", "nearray (nearray nat)",
                 (xss) => L.view (compose (ix (0)) (ix (0))) (xss) === xss[0][0]);
});
