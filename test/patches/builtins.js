// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {describe} from "mocha";
import jsc from "jsverify";

import {patchBuiltins} from "@fpjs/overture/patches";

import {compose, constant, uncurry} from "@fpjs/overture/base";

import {concat, empty} from "@fpjs/overture/algebras/monoid";
import {equals} from "@fpjs/overture/algebras/setoid";
import {map} from "@fpjs/overture/algebras/functor";
import {ap, pure} from "@fpjs/overture/algebras/applicative";
import {chain} from "@fpjs/overture/algebras/chain";
import {foldl} from "@fpjs/overture/algebras/foldable";
import {traverse} from "@fpjs/overture/algebras/traversable";
import {Identity} from "@fpjs/overture/data/identity";

import {inc, sum} from "../test-utils/functions.js";

patchBuiltins();

describe ("Array", () => {
    jsc.property("Setoid identity", "array nat",
                 (xs) => equals (xs) ([...xs]));

    jsc.property("Setoid difference", "nearray nat",
                 (xs) => ! equals (xs) (xs.map(inc)));

    jsc.property("Semigroup", "array nat", "array nat",
                 (xs, ys) => equals (concat (xs) (ys)) ([...xs, ...ys]));

    jsc.property("Monoid", "array nat",
                 (xs) => equals (concat (xs) (empty (Array))) (xs));

    jsc.property("Functor", "array nat",
                 (xs) => equals (map (inc) (xs)) (xs.map(inc)));

    jsc.property("Apply", "array nat",
                 (xs) => equals (ap ([inc, inc]) (xs)) ([...xs.map(inc), ...xs.map(inc)]));

    jsc.property("Applicative", "nat",
                 (x) => equals (pure (Array) (x)) ([x]));

    jsc.property("Chain", "array nat",
                 (xs) => equals (chain ((n) => [n]) (xs)) (xs));

    jsc.property("Foldable", "array nat",
                 (xs) => foldl (sum) (0) (xs) === xs.reduce(uncurry (sum), 0));

    jsc.property("Traversable", "array nat",
                 (xs) => equals (traverse (Identity) (compose (Identity) (inc)) (xs)) (Identity (map (inc) (xs))));
});

describe ("Function", () => {
    jsc.property("Functor", "fn nat", "nat",
                 (f, x) => map (inc) (f) (x) === inc (f (x)));
});

describe ("Promise", () => {
    jsc.property("Functor", "nat",
                 (x) => map (constant (true)) (Promise.resolve (x)));

    jsc.property("Apply", "nat",
                 (x) => ap (Promise.resolve (constant (true))) (Promise.resolve (x)));

    jsc.property("Applicative", "nat",
                 (x) => ap (pure (Promise) ((y) => y === x)) (pure (Promise) (x)));
});
