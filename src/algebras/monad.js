// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

/**
 * Monad is an applicative functor, with a sequential composition
 * operation. Hence an instance of Monad must adhere to the Applicative and
 * Chain specifications.
 */

import {isApplicative} from "@fpjs/overture/algebras/applicative";
import {isChain} from "@fpjs/overture/algebras/chain";

///:: a -> boolean
export const isMonad = (x) => isApplicative (x) && isChain (x);

export {chain} from "@fpjs/overture/algebras/chain";
export {pure} from "@fpjs/overture/algebras/applicative";
