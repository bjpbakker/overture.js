// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {describe, it} from "mocha";
import {assert} from "chai";
import jsc from "jsverify";

import {
    id, compose, type, isPrimitive,
    apply, rapply, curry, uncurry, flip, constant, always, not,
    fanout,
} from "@fpjs/overture/base";

import {
    equals,
} from "@fpjs/overture/algebras/setoid";

import {sum} from "../test-utils/functions.js";

describe ("id", () => {
    jsc.property("identity", "json", (x) => id (x) === x);
});

describe ("compose", () => {
    jsc.property("id . id ≡ id", "json", (x) => compose (id) (id) (x) === id (x));
    jsc.property("(f . g) x ≡ f (g (x))", "fun nat", "fun nat", "nat",
                 (f, g, n) => compose (f) (g) (n) === f (g (n)));
});

describe ("type", () => {
    jsc.property("string", "string", (s) => type(s) === "String");
    jsc.property("number", "number", (n) => type(n) === "Number");
    jsc.property("object", "{ x: json }", (x) => type(x) === "Object");
});

describe ("isPrimitive", () => {
    jsc.property("number", "number", (n) => isPrimitive (n));
    jsc.property("boolean", "bool", (b) => isPrimitive (b));
    jsc.property("string", "string", (s) => isPrimitive (s));
    jsc.property("function", "fn json", (f) => ! isPrimitive (f));

    it ("special values", () => {
        assert (isPrimitive (null), "null");
        assert (isPrimitive (undefined), "undefined");
        assert (isPrimitive (NaN), "NaN");
        assert (isPrimitive (Infinity), "Infinity");
    });
});

describe ("apply", () => {
    jsc.property("apply (f) (x) ≡ f (x)", "nat",
                 (x) => apply (id) (x) === id (x));
});

describe ("rapply", () => {
    jsc.property("rapply (x) (f) ≡ f (x)", "nat",
                 (x) => rapply (x) (id) === id (x));
});

describe ("curry", () => {
    const f = (x, y) => x + y;

    jsc.property("f (x, y) ≡ curry (f) (x) (y)", "nat", "nat",
                 (x, y) => f (x, y) === curry (f) (x) (y));
});

describe ("uncurry", () => {
    const f = (x) => (y) => x + y;

    jsc.property("f (x) (y) ≡ uncurry (f) (x, y)", "nat", "nat",
                 (x, y) => f (x) (y) === uncurry (f) (x, y));
});

describe ("flip", () => {
    const f = (x) => (y) => x - y;

    jsc.property("f (x) (y) ≡ flip (f) (y) (x)", "nat", "nat",
                 (x, y) => f (x) (y) === flip (f) (y) (x));
});

describe ("constant", () => {
    jsc.property("K-combinator", "nat", "nat",
                 (x, y) => constant (x) (y) === x);
});

describe ("always", () => {
    it("is alias of constant", () => always === constant);
});

describe ("not", () => {
    jsc.property("inverse", jsc.bool,
                 (x) => (x ^ not (x)) === 1);
});

describe ("fanout", () => {
    jsc.property("fanout (id) (id) (x) ≡ (x, x)", "nat",
                 (x) => equals (fanout (id) (id) (x)) ([x, x]));

    jsc.property("fanout (sum (1)) (sum (2)) (x) ≡ (x+1, x+2)", "nat",
                 (x) => equals (fanout (sum (1)) (sum (2)) (x)) ([x+1, x+2]));
});
