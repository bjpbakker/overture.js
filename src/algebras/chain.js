// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

/**
 * Chain sequentially composes two actions, passing any value produced by the
 * first as an argument to the second.
 */

import {id, type} from "@fpjs/overture/base";
import {isApply} from "@fpjs/overture/algebras/apply";

///:: a -> boolean
export const isChain = (x) => isApply (x) && "fantasy-land/chain" in x;

//:: Chain m => (a -> m b) -> m a -> m b
export const chain = (ab) => (m) => {
    const op = m["fantasy-land/chain"];
    if (op === undefined) {
        throw TypeError(`'${type(m)}' is not an Chain.`);
    }
    return op.call(m, ab);
};

///:: Chain m => m (m a) -> m a
export const join = chain (id);
