// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

/**
 * Semigroup is an algebraic structure consisting of a set together with an
 * associative binary operation.
 */

import {type} from "@fpjs/overture/base";

///:: a -> boolean
export const isSemigroup = (x) => "fantasy-land/concat" in x;

///:: Semigroup a => a -> a -> a
export const concat = (x) => (y) => {
    const op = x["fantasy-land/concat"];
    if (op === undefined) {
        throw TypeError(`'${type(x)}' is not a Semigroup.`);
    }
    return op.call(x, y);
};
