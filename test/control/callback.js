// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {describe, it} from "mocha";
import {assert} from "chai";

import {inc} from "../test-utils/functions.js";

import {compose, type} from "@fpjs/overture/base";
import {map} from "@fpjs/overture/algebras/functor";
import {ap, pure} from "@fpjs/overture/algebras/applicative";
import {chain} from "@fpjs/overture/algebras/monad";

import {
    Callback, error, recover, runCallback,
} from "@fpjs/overture/control/callback";

describe ("Callback", () => {
    const callback = pure (Callback);
    const runAssert = (f) => (cb) => (done) => runCallback ((err, actual) => {
        if (err != null) {
            done(err);
        } else {
            try {
                done(null, f(actual));
            } catch (e) {
                done(e);
            }
        }
    }) (cb);
    const runError = (f) => (cb) => (done) =>
          runCallback ((err, actual) => {
              if (err != null) {
                  try {
                      done(null, f(err));
                  } catch (e) {
                      done(e);
                  }
              } else {
                  done(`Error was expected, but got value ${actual}.`);
              }
          }) (cb);

    it ("type", () =>
        assert.equal((type (callback (42))), "Callback"));

    it ("Functor", runAssert
        (result => assert.equal(result, 43))
        (map (inc) (callback (42))));

    it ("Apply", runAssert
        (result => assert.equal(result, 43))
        (ap (callback(inc)) (callback (42))));

    it ("Applicative", runAssert
        (result => assert.equal(result, 42))
        (pure (Callback) (42)));

    it ("Monad", runAssert
        (result => assert.equal(result, 43))
        (chain (compose (callback) (inc)) (callback (42))));

    it ("runCallback throws TypeError for non-Callback",
        () => assert.throws(() => runCallback (assert.fail) ({}), TypeError));

    describe ("error", () => {
        it ("returns a Callback with an error", runError
            (e => assert.equal(e, "Expected error"))
            (error ("Expected error")));
    });

    describe ("recover", () => {
        it ("recovers from an error", runAssert
            (result => assert.equal(result, 42))
            (recover ((_) => callback (42)) (error ("ENOENT"))));

        it ("returns error when it cannot recover", runError
            (e => assert.equal(e, "EEXIST"))
            (recover (error) (error ("EEXIST"))));

        it ("throws TypeError for non-Callback",
            () => assert.throws(() => recover (assert.fail) ({})));
    });
});
