// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

/**
 * The identity functor and monad.
 */

import {type} from "@fpjs/overture/base";
import {map} from "@fpjs/overture/algebras/functor";
import {equals} from "@fpjs/overture/algebras/setoid";
import {lte} from "@fpjs/overture/algebras/ord";

///:: a -> Identity a
export const Identity = (x) => ({
    "@@type": "Identity",
    "constructor": Identity,
    "runIdentity": x,
    "fantasy-land/map": (f) => Identity (f (x)),
    "fantasy-land/ap": map ((f) => f(x)),
    "fantasy-land/chain": (f) => f (x),
    "fantasy-land/reduce": (f, z) => f (z, x),
    "fantasy-land/equals": (y) => equals (x) (y.runIdentity),
    "fantasy-land/lte": (y) => lte (x) (y.runIdentity),
    "fantasy-land/traverse": (F, f) => map (Identity) (f (x)),
});
Identity["fantasy-land/of"] = Identity;

/** Extract the value from an Identity functor. */
///:: Identity a -> a
export const runIdentity = (o) => {
    if (! ("runIdentity" in o)) {
        throw TypeError(`${type(o)} is not Identity`);
    }
    return o["runIdentity"];
};
