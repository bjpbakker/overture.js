// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

/**
 * Traversable represents a class of data structures that can be traversed from
 * left to right, performing an action on each element.
 */

import {flip, id, type} from "@fpjs/overture/base";

///:: a -> boolean
export const isTraversable = (x) => "fantasy-land/traverse" in x;

///:: Applicative f, Traversable t => TypeRep f -> (a -> f b) -> t a -> f (t b)
export const traverse = (F) => (ab) => (x) => {
    const op = x["fantasy-land/traverse"];
    if (op === undefined) {
        throw TypeError(`'${type(x)}' is not Traversable.`);
    }
    return op.call(x, F, ab);
};

///:: Applicative f, Traversable t => TypeRep f -> t (f a) -> f (t a)
export const sequence = flip (traverse) (id);
