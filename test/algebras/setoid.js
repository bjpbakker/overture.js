// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {describe, it} from "mocha";
import jsc from "jsverify";

import {sum} from "../test-utils/arbitrary.js";

import {isSetoid, eq, equals, ineq} from "@fpjs/overture/algebras/setoid";

describe ("Setoid", () => {
    describe("equals", () => {
        jsc.property("equality", sum(jsc.nat), sum(jsc.nat),
                     (x, y) => equals (x) (y) === (x.getSum === y.getSum));

        jsc.property("throws on non-setoid", jsc.oneof(sum(jsc.nat), jsc.record({x: "nat"})),
                     (x) => jsc.throws(() => equals (x) (x), TypeError, "'Object' is not a Setoid.") !== isSetoid(x));

    });

    describe("eq", () => {
        it("is alias of equals", () => eq === equals);
    });

    describe("ineq", () => {
        jsc.property("inequality", sum(jsc.nat), sum(jsc.nat),
                     (x, y) => ineq (x) (y) !== eq (x) (y));
    });

    describe("isSetoid", () => {
        jsc.property("value w/ equals", sum(jsc.nat),
                     (x) => isSetoid (x));

        jsc.property("value w/o equals", jsc.record({x: "json"}),
                     (x) => ! isSetoid (x));
    });

    describe("primitive derivation", () => {
        jsc.property("derive primitive equality", "nat", "nat",
                     (x, y) => equals (x) (y) === (x === y));

        jsc.property("primitive", jsc.oneof(jsc.nat, jsc.string, jsc.bool),
                     (x) => isSetoid (x));
    });

    describe("ord derivation", () => {
        const Ordered = (x) => ({
            "@@type": "Ordered",
            "getOrdered": x,
            "fantasy-land/lte": (y) => x <= y.getOrdered,
        });

        jsc.property("derive equality from lte", "nat", "nat",
                     (x, y) => equals (Ordered (x)) (Ordered (y)) === (x === y));
    });
});
