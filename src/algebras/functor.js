// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

/**
 * Functor is a map between categories.
 */

import {compose, constant, flip, type} from "@fpjs/overture/base";

///:: a -> boolean
export const isFunctor = (x) => "fantasy-land/map" in x;

///:: Functor f => (a -> b) -> f a -> f b
export const map = (ab) => (a) => {
    const fmap = a["fantasy-land/map"];
    if (fmap === undefined) {
        throw TypeError(`'${type(a)}' is not a Functor.`);
    }
    return fmap.call(a, ab);
};

/** Replace the input with the same fixed value. */
///:: Functor f => a -> f b -> f a
export const replace = compose (map) (constant);
/** Flipped version of `replace`. */
///:: Functor f => f a -> b -> f b
export const replicate = flip (replace);
