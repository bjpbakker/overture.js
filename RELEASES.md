# Releases

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic
Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## Fixed

- Set `name` attribute for `Maybe` to allow for more informative error messages.
- Fix `Ord` and `Setoid` properties of `Maybe`.

### Changed

- Require nodejs 18.x or higher (possibly breaking)

## [v0.7.1] - 2023-10-22

### Added

- `repl` command to jump in an overture repl from any project with:
  `npx --package=@fpjs/overture -c repl`

### Fixed

- `repl` has been fixed and moved to `bin`

## [v0.7.0] - 2023-10-18

### Added

- Applicative: `lift`
- Callback: add `recover` and `error`
- Maybe: add `maybeToArray`

### Changed

- Require nodejs 16.x or higher (possibly breaking)
- Moved `lift2` from `Apply` to `Applicative` (possibly breaking)

## [v0.6.1] - 2022-05-08

### Fixed

- Fix `/algebras/*` CJS imports

## [v0.6.0] - 2021-11-20

### Changed

- Require nodejs 12.x or higher (possibly breaking)
- Change package type to `module`
- Replace babel root-imports with the package name
- Move cjs build into `dist/cjs`

## [v0.5.2] - 2021-05-29

### Fixed

- Fixup packaging mistake; include root-level modules in published package

## [v0.5.1] - 2021-05-29

### Fixed

- Fix package structure for older node versions

## [v0.5.0] - 2021-05-28

### Added

- Functor: `replace`, `replicate`
- Ord: `min`, `max`, `clamp`
- Setoid: `eq`, `ineq`

### Changed

- Rename default branch to `main`
- Gitlab: run CI on all supported node versions (10, 12, 14, 15, 16)
- Build output to dist and use package exports
- Run tests in build script (prevents publishing w/ failing tests)

## [v0.4.1] - 2021-03-10

### Added

- Base: boolean `not`
- Foldable: `foldMap`, `filter`, `filterNot`

### Fixed

- Include `build.sh` in published package

## [v0.4.0] - 2020-08-20

### Added

- Chain: `join`

### Changed

- Maybe: rename `Maybe.match` to `Maybe.caseOf` (possibly breaking)

## v0.3.0 - 2020-01-03

### Added

- Algebras: `Ord`, `Foldable`, `Traversable`
- Data types: `Maybe`
- Setoid: derive from `Ord`
- Apply: `lift2`
- repl: script to provide loaded Overture in REPL (`~/repl`)

## v0.2.0 - 2019-09-13

### Added

- Algebras: `Monoid`, `Monad`

### Changed

- Restructured project layout (breaking)

## v0.1.4 - 2019-05-31

Initial release.

### Added

- Algebras: `Setoid`, `Functor`, `Applicative`, `Chain`
- Data types: `Identity`, `Const`
- Algebras for builtins: `Array`, `Function`, `Promise`
- Lenses

[v0.4.0]: https://gitlab.com/bjpbakker/overture.js/-/tags/v0.4.0
[v0.4.1]: https://gitlab.com/bjpbakker/overture.js/-/compare/v0.4.0...v0.4.1
[v0.5.0]: https://gitlab.com/bjpbakker/overture.js/-/compare/v0.4.1...v0.5.0
[v0.5.1]: https://gitlab.com/bjpbakker/overture.js/-/compare/v0.5.0...v0.5.1
[v0.5.2]: https://gitlab.com/bjpbakker/overture.js/-/compare/v0.5.1...v0.5.2
[v0.6.0]: https://gitlab.com/bjpbakker/overture.js/-/compare/v0.5.2...v0.6.0
[v0.6.1]: https://gitlab.com/bjpbakker/overture.js/-/compare/v0.6.0...v0.6.1
[v0.7.0]: https://gitlab.com/bjpbakker/overture.js/-/compare/v0.6.1...v0.7.0
[v0.7.1]: https://gitlab.com/bjpbakker/overture.js/-/compare/v0.7.0...v0.7.1
[unreleased]: https://gitlab.com/bjpbakker/overture.js/-/compare/v0.7.1...main
