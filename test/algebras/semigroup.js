// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {describe} from "mocha";
import jsc from "jsverify";

import {sum} from "../test-utils/arbitrary.js";

import {concat, isSemigroup} from "@fpjs/overture/algebras/semigroup";

describe ("Semigroup", () => {
    describe("concat", () => {
        jsc.property("concat", sum(jsc.nat), sum(jsc.nat),
                     (x, y) => (concat (x) (y)).getSum === x.getSum + y.getSum);

        jsc.property("throws on non-semigroup", jsc.oneof(sum(jsc.nat), jsc.record({x: "nat"})),
                     (x) => jsc.throws(() => concat (x) (x), TypeError, "'Object' is not a Semigroup.") !== isSemigroup (x));
   });

    describe("isSemigroup", () => {
        jsc.property("value w/ concat", sum(jsc.nat),
                     (x) => isSemigroup (x));

        jsc.property("value w/o concat", jsc.record({x: "json"}),
                     (x) => ! isSemigroup (x));
    });
});
