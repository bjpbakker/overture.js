// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {describe} from "mocha";
import jsc from "jsverify";

import {identity, sum} from "../test-utils/arbitrary.js";

import {id, constant} from "@fpjs/overture/base";
import {runIdentity} from "@fpjs/overture/data/identity";

import {map, isFunctor, replace, replicate} from "@fpjs/overture/algebras/functor";

describe ("Functor", () => {
    describe("map", () => {
        jsc.property("map", identity(jsc.nat), jsc.string,
                     (x, y) => runIdentity (map (constant (y)) (x)) === y);

        jsc.property("throws on non-functor", jsc.oneof(identity(jsc.nat), jsc.record({x: jsc.nat})),
                     (x) => jsc.throws(() => map (id) (x), TypeError, "'Object' is not a Functor.") !== isFunctor (x));
    });

    describe("isFunctor", () => {
        jsc.property("identity functor", identity(jsc.nat),
                     (x) => isFunctor (x));

        jsc.property("non-functor", sum(jsc.nat),
                     (x) => ! isFunctor (x));
    });

    describe("replace", () => {
        jsc.property("with fixed value", identity(jsc.nat), jsc.string,
                     (x, y) => runIdentity (replace (y) (x)) === y);
    });

    describe("replicate", () => {
        jsc.property("a fixed value", identity(jsc.nat), jsc.string,
                     (x, y) => runIdentity (replicate (x) (y)) === y);
    });
});
